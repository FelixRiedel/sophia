#pragma once

typedef float f32;
typedef double f64;

typedef unsigned char u8;
typedef signed char i8;

typedef unsigned short u16;
typedef signed short i16;
typedef unsigned int u32;
typedef signed int i32;
typedef unsigned long u64;
typedef signed long i64;

#if defined(__gcc__) || defined(__clang__)
#define STATIC_ASSERT _Static_assert
#else
#define STATIC_ASSERT static_assert
#endif

STATIC_ASSERT(sizeof(u8) == 1, "Wrong byte size for u8");
STATIC_ASSERT(sizeof(u16) == 2, "Wrong byte size for u16");
STATIC_ASSERT(sizeof(u32) == 4, "Wrong byte size for u32");
STATIC_ASSERT(sizeof(u64) == 8, "Wrong byte size for u64");

STATIC_ASSERT(sizeof(i8) == 1, "Wrong byte size for i8");
STATIC_ASSERT(sizeof(i16) == 2, "Wrong byte size for i16");
STATIC_ASSERT(sizeof(i32) == 4, "Wrong byte size for i32");
STATIC_ASSERT(sizeof(i64) == 8, "Wrong byte size for i64");

STATIC_ASSERT(sizeof(f32) == 4, "Wrong byte size for f32");
STATIC_ASSERT(sizeof(f64) == 8, "Wrong byte size for f64");