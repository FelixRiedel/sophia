#pragma once

/**
 * Functions from this file should only be called by the sophia math header. The functions will assume the inputs are right
 * in dimensions and format (should be checked prior to their call).
 */

namespace sophia::compute {
    float* matrix_dot_product(float*, float*, unsigned int, unsigned int, unsigned int);
}