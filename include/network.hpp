#pragma once

#include "layer.hpp"
#include "math.hpp"

namespace sophia {
    class Net {
    private:

    public:
        void learn(Mat& input, Mat& target);

        Mat predict(Mat& input);
    };
}