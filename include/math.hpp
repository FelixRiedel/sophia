#pragma once

#include "types.hpp"
#include "defines.hpp"

#include "compute.hpp"

namespace sophia {




    class Mat {
    private:
        u32 _rows;
        u32 _cols;
        f32* _elements;
    public:
        explicit Mat(u32);
        Mat(u32, u32);
        Mat(f32*, u32, u32);
        Mat(Mat&);

        ~Mat();

        sophia::Mat elementwise_product(const Mat&);

        void operator*=(const Mat&);
        void operator*=(float);

        f32* operator()(u32, u32) const;
        f32* data();
        u32 cols() const;
        u32 rows() const;

        static Mat zeros(u32);
        static Mat zeros(u32, u32);

        static Mat eye(u32);
    };

    Mat operator*(Mat&, Mat&);

    Mat operator*(const Mat&, float);

    Mat operator*(float, const Mat&);

}