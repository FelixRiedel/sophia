#pragma once

#include <vector>
#include "types.hpp"

namespace sophia {
    template <typename T>
    class Layer {
    private:
        std::vector<u32> _input_size;
        std::vector<u32> _output_size;
    public:
        Layer();
        Layer(std::vector<u32> input_size);
        Layer(std::vector<u32> input_size, std::vector<u32> output_size);
    };
}