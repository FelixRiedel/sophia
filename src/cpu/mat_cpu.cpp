#include <cstdlib>
#include "../../include/compute.hpp"

/*
 * Naïve algorithm, todo : make a more performant implementation
 */
float* sophia::compute::matrix_dot_product(float* A, float* B, unsigned int m_size, unsigned int n_cols, unsigned int n_rows) {
    unsigned int size = n_cols*n_rows;
    float* C = (float*) malloc(size*sizeof(float));

    for(size_t i = 0; i < size; i++) {
        float r = 0;
        for(size_t k = 0; k < m_size; k++) {
            r += A[k + (i/n_cols)*m_size]*B[k*n_cols + i%n_cols];
        }
        C[i] = r;
    }
    return C;
}