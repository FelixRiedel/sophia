#include "../../include/math.hpp"
#include <iostream>
#include <cstring>

sophia::Mat::Mat(u32 size) : Mat(size, size) {}

sophia::Mat::Mat(sophia::Mat& M) : Mat(M._cols, M._rows) {
    memcpy(_elements, M._elements, _rows*_cols*sizeof(f32));
}

sophia::Mat::Mat(u32 cols, u32 rows) : _rows(rows), _cols(cols) {
    _elements = (f32*) malloc(_rows*_cols*sizeof(f32));
}

sophia::Mat::Mat(f32* buffer, u32 cols, u32 rows) : _elements(buffer), _rows(rows), _cols(cols) {}

sophia::Mat::~Mat() {
    free(_elements);
}

sophia::Mat sophia::Mat::elementwise_product(const sophia::Mat& mat) {
#if SOPHIA_VERBOSE
    if(_rows != mat._rows || _cols != mat._cols) {
        std::cerr << "Error in matrix dot product : dimensions of left operand (" << _rows << ", " << _cols <<
                  ") does not match right operand (" << mat._rows << ", " << mat._cols << ")\n";
    }
#endif
    Mat out(_cols, _rows);

    for(u64 k = 0; k < _cols; k++) {
        for(u64 l = 0; l < _cols; l++) {
            *out(k, l) = (*operator()(k, l))*(*mat(k, l));
        }
    }
    return out;
}

f32* sophia::Mat::operator()(const u32 pos_x, const u32 pos_y) const {
    if(pos_x > _cols || pos_y > _rows) {
        std::cerr << "Error in matrix access at (" << pos_x << ", " << pos_y <<
                  "), matrix is of size (" << _rows << ", " << _cols << ")\n";
    }
    return _elements + pos_y*_cols + pos_x;
}

sophia::Mat sophia::Mat::zeros(u32 size) {
    return zeros(size, size);
}
sophia::Mat sophia::Mat::zeros(u32 cols, u32 rows) {
    Mat out(cols, rows);
    for(u64 k = 0; k < cols*rows; k++) {
        out._elements[k] = 0.0f;
    }

    return out;
}

sophia::Mat sophia::Mat::eye(u32 size) {
    Mat out(size);
    for(u64 k = 0; k < size; k++) {
        out._elements[k + k*out._cols] = 1.0f;
    }
    return out;
}

f32* sophia::Mat::data() {
    return _elements;
}
u32 sophia::Mat::cols() const {
    return _cols;
}
u32 sophia::Mat::rows() const {
    return _rows;
};

sophia::Mat sophia::operator*(sophia::Mat& A, sophia::Mat& B) {
    if(A.rows() != B.cols()) {
        std::cerr << "Operand A's number of rows should match operand B's number of columns in A*B\n";
        return sophia::Mat::eye(1);
    }
    float* buffer = sophia::compute::matrix_dot_product(A.data(), B.data(), A.cols(), B.cols(), A.rows());
    sophia::Mat out(buffer, B.cols(), A.rows());
    return out;
}