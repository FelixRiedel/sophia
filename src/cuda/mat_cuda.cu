#include <stdio.h>

#include "../../include/compute.hpp"

/**
 * Naive matrix dot product using the GPU
 * @param A first operand
 * @param B second operand
 * @param C output
 * @param N common dimension between A and B
 * @param w width of B
 * @param size size of C (width*height)
 */
__global__ void matrix_dot(const float* A, const float* B, float* C, unsigned int N, unsigned int w, unsigned int size) {
    unsigned int i = threadIdx.x;
    if(i > size) return;
    float r = 0;
    for(size_t k = 0; k < N; k++) {
        r += A[k + (i/w)*N]*B[k*w + i%w];
    }
    C[i] = r;
}

float* sophia::gpu::gpu_matrix_dot_product(float* A, float* B, unsigned int m_size, unsigned int n_cols, unsigned int n_rows) {
    unsigned int size = n_cols*n_rows;
    float* C = (float*) malloc(size*sizeof(float));

    float *d_C, *d_A, *d_B;
    cudaMalloc(&d_C, size*sizeof(float));
    cudaMalloc(&d_A, m_size*n_rows*sizeof(float));
    cudaMalloc(&d_B, m_size*n_cols*sizeof(float));

    cudaMemcpy(d_A, A, m_size*n_rows*sizeof(float), cudaMemcpyHostToDevice);
    cudaMemcpy(d_B, B, m_size*n_cols*sizeof(float), cudaMemcpyHostToDevice);

    const unsigned int THREADS_CTA = 1 << 10; // todo : better CTA size selection
    unsigned int N_BLOCKS = (size + THREADS_CTA - 1) / THREADS_CTA;

    matrix_dot<<<N_BLOCKS, THREADS_CTA>>>(d_A, d_B, d_C, m_size, n_cols, size);

    cudaMemcpy(C, d_C, size*sizeof(float), cudaMemcpyDeviceToHost);

    cudaFree(d_A);
    cudaFree(d_B);
    cudaFree(d_C);

    return C;
}
