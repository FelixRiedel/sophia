/*
 * 2023 Felix Riedel, MIT license.
 */

#include <utility>

#include "../../include/layer.hpp"

template<typename T>
sophia::Layer<T>::Layer() {

}

template<typename T>
sophia::Layer<T>::Layer(std::vector<u32> input_size) : _input_size(std::move(input_size)) {

}

template<typename T>
sophia::Layer<T>::Layer(std::vector<u32> input_size, std::vector<u32> output_size) : _input_size(std::move(input_size)), _output_size(std::move(output_size)) {

}