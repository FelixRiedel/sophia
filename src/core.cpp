/*
 * 2023 Felix Riedel, MIT license.
 */

#include "../include/sophia.hpp"

const char* sophia::lib_version() {
    return "0.0.1";
}