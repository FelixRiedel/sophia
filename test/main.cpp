/*
 * 2023 Felix Riedel, MIT license.
 */

#include "sophia.hpp"
#include "math.hpp"
#include <iostream>

void display_matrix (sophia::Mat& A, std::basic_ostream<char>& buf = std::cout) {
    for(size_t k = 0; k < A.rows(); k++) {
        buf << "| ";
        for(size_t l = 0; l < A.cols(); l++) {
            buf << *A(l, k) << " ";
        }
        buf << "|\n";
    }
}

bool assert_equal(sophia::Mat& A, sophia::Mat& B) {
    if(A.cols() != B.cols() || A.rows() != B.rows())
        return false;
    bool correct = true;
    for(size_t k = 0; k < A.rows(); k++) {
        for(size_t l = 0; l < A.cols(); l++) {
            if(*A(l, k) != *B(l, k)) {
                correct = false;
            }
        }
    }
    return correct;
}

bool test_matrix_dot_product_1() {
    sophia::Mat A = sophia::Mat::zeros(3);
    sophia::Mat B = sophia::Mat::zeros(3);

    *A(2, 0) = 1;
    *A(2, 1) = 2;
    *A(1, 0) = 3;
    *A(0, 1) = 4;
    *A(1, 2) = 5;
    *A(1, 1) = 6;

    *B(2, 2) = 1;
    *B(2, 1) = 2;
    *B(1, 1) = 3;
    *B(0, 1) = 4;
    *B(1, 2) = 5;
    *B(1, 0) = 6;

    sophia::Mat C = A*B;

    sophia::Mat C_true = sophia::Mat::zeros(3);
    *C_true(0, 0) = 12; *C_true(1, 0) = 14; *C_true(2, 0) = 7;
    *C_true(0, 1) = 24; *C_true(1, 1) = 52; *C_true(2, 1) = 14;
    *C_true(0, 2) = 20; *C_true(1, 2) = 15; *C_true(2, 2) = 10;

    bool correct = assert_equal(C_true, C);

    if(!correct) {
        std::cerr << "[FAIL] Test : test_matrix_dot_product_1 failed, expected result : \n";
        display_matrix(C_true, std::cerr);
        std::cerr << "Result given : \n";
        display_matrix(C, std::cerr);
    } else {
        std::cout << "[SUCCESS] Test : test_matrix_dot_product_1\n";
    }

    return correct;
}

bool test_matrix_dot_product_2() {
    sophia::Mat A(3, 1);
    sophia::Mat B(1, 3);

    *A(0, 0) = 1;
    *A(1, 0) = 2;
    *A(2, 0) = 3;

    *B(0, 0) = 5;
    *B(0, 1) = 2;
    *B(0, 2) = 1;

    sophia::Mat C = A*B;

    sophia::Mat C_true = sophia::Mat::zeros(1);
    *C_true(0, 0) = 12;

    bool correct = assert_equal(C_true, C);

    if(!correct) {
        std::cerr << "[FAIL] Test : test_matrix_dot_product_2 failed, expected result : \n";
        display_matrix(C_true, std::cerr);
        std::cerr << "Result given : \n";
        display_matrix(C, std::cerr);
    } else {
        std::cout << "[SUCCESS] Test : test_matrix_dot_product_2\n";
    }

    return correct;
}

int main() {
    std::cout << "Using sophia Deep Learning library version : " << sophia::lib_version() << std::endl;

    size_t errors = 0;

    errors += !test_matrix_dot_product_1();
    errors += !test_matrix_dot_product_2();

    if(errors > 0) {
        std::cerr << "Test finished with " << errors << " errors. Check the logs for more detail.\n";
    } else {
        std::cout << "Test finished without errors.\n";
    }

    return 0;
}
